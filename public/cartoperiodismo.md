title: Cartoperiodismo
subtitle: Introducción al diseño cartográfico
class: animation-fade
autor: Alejandro Zappala
layout: true
---

![portada](./img/portada.jpg)

---

class: center,middle

# {{title}}
----
## [{{subtitle}}]



---

# Cartoperiodismo

1. De verdad, ¿hace falta un mapa?
2. ¿Para qué es tu mapa?
3. Datos cartografiables
4. Herramientas
5. Marco geográfico
6. Generalidades del diseño cartográfico
7. Simbolización cartográfica
8. Publicación
9. Acerca de servicios online de terceros
10. Buenas prácticas

---

## De verdad, ¿hace falta un mapa?
----

![Población de Renos](./img/MoosePopulation.jpg)

@TerribleMaps

---

class: center, middle

## De verdad, ¿hace falta un mapa?
----
![Sistema Métrico por Países](./img/SistemaMetrico.jpg)

Sistema Métrico por Países

---
## De verdad, ¿hace falta un mapa?
----

[![Instagram Friendly](./img/InstagramFriendly.jpg)](https://www.splash-maps.com/first-instagram-friendly-map/)

???
We’ve created our first custom-made Instagram friendly map in the UK together with Clarion Communications. The 6 islands of the Outer Hebrides are already in Lonely’s Planet’s top 5 regions to visit in 2019. With a disproportionate number of beauty spots, our map is aimed at attracting visitors to the exact spot to take that iconic Instagram selfie or landscape. With the full detail of an Ordnance Survey 1:50k scale Landranger map and printed on our signature weatherproof fabrics you’ll be tempted to explore on foot or bike, whatever the weather.

---

## ¿Para qué es tu mapa?
----
![Irán quiere Guerra](./img/IranWantsWar.jpg)

???
"La calidad de un mapa es frecuentemente una cuestión de perspectiva, más que de diseño"
--> mapas distintos para representar cosas distintas en la misma zona.

---
## ¿Para qué es tu mapa?
----

![Not On My Watch](./img/NotOnMyWatch.jpg)


---
## ¿Cuál es su audiencia?
----

![Winnie The Pooh Map.jpg](./img/WinnieThePoohMap.jpg)

???
- General
- Especializada
- Infantil

---
## Contexto físico
----

- ¿Para qué medio se va a crear el mapa? - Libro, diario impreso, hoja de papel, póster,  web, ...
- Noticia puntual `VS` Reportaje de fondo
- Entrada de Blog `VS` Web interactiva
- Impreso `VS` pantalla luminescente (donde cambian las reglas de color y de contraste)
- Si convive con `otros formatos`: Fotos, video, animaciones, histogramas, diagramas...
- `Dispositivo final` (móvil, tableta, ebook, PC, portatil...)
- Resoluciones y formatos

---

## Datos cartografiables
----

![Población per cápita](./img/populationpercapita.jpg)

---
## Datos cartografiables
----

- Un `fenómeno` es algo que ocurre en el mundo real
- Un `dato` es un registro de observación del fenómeno
- `Los mapas muestran datos, no fenómenos`
---

## El cuidado con los datos
----
Trata con mucho cuidado los datos que estas cartografiando
- Cómo se relaciona con el mundo que quieren representar
- Cómo de parecidos o diferentes son con la realidad y cómo eso puede afectar la comprensión del fenómeno
- De dónde salen, quién los ha recopilado, cómo lo ha hecho
- Busca siempre el `origen de los datos`.
- No reutilices datos ajenos reutilizados a no ser que `puedas comprobar todo el proceso` al que han sido sometidos

---

## Tipos de dato: Individuales o agregados
----
![Individuales o agregados](./img/IndividualAggregate.jpg)

---

## Tipos de dato: Continuo o discreto
----
![Continuo o discreto](./img/ContinuousDiscrete.jpg)



---
## Fuentes de datos Primarias
----
- Personal técnico haciendo una recolección de datos de inspección de edificios
- Sistemas de Posicionamiento GPS
- Teléfonos móviles
- Recopilación de datos de mapas existentes
- Imágenes de Teledetección (Fotografía aérea, LIDAR, imágenes multibanda de satélite)
- Crowdsourcing ([openstreetmap](http://openstreetmap.org))

---

## Infraestructuras de Datos Espaciales
----
Datos geográficos oficiales de referencia en `formatos abiertos normalizados`

- Vienen de la Directiva europea [INSPIRE](http://inspire.ec.europa.eu/inspire-your-country-map/27543)
- Existe una [IDEE](https://www.europeandataportal.eu/) con una evolución de más de 20 años donde encontrar toda la información oficial abierta disponible en el contexto europeo
- Los datos geográficos oficiales nacionales se encuentran en el [geoportal del CNIG](http://centrodedescargas.cnig.es/CentroDescargas/index.jsp)
- Se elaboran por comunidades autónomas. En Valencia: [Geoportal del ICV](http://www.icv.gva.es/)

---
##  Información Geográfica de Referencia
----

![CartoBaseINE](./img/CartoBaseINE.jpg)


---
## Mapas vectoriales del IGN
----

[![MTN y BCN del IGN](./img/MTNBCN.jpg)](http://centrodedescargas.cnig.es/CentroDescargas/index.jsp)

???
- Bases Topográficas: Información topográfica con la precisión correspondiente de su escala
- Bases Cartográficas: Tras pasar procesos de generalización, simbolización y rotulación

---

class: center, middle

# Herramientas
----

---

## Sin computadora: a mano alzada
----

![Hand Drawn Map Association](./img/HandDrawnMapAssociation.jpg)

---
## Sin computadora: Old School
----
![OldSchoolMappingTools](./img/OldSchoolMappingTools.jpg)

???
Mesa iluminada, escuadra y cartabón; tiralíneas, papel cebolla, plantillas y cinta adhesiva

---
## Sin computadora: Old School
----

![Lord Of The Ring](./img/LordOfTheRing.jpg)

---
## Herramientas *online* de análisis y visualización de datos
----

**Histogramas, etc**

- [Tableau](https://www.tableau.com/). Tiene una [versión gratuita](https://public.tableau.com/s/resources)
- [Datawrapper](https://www.datawrapper.de/):Muy popular y de fácil uso para visualizaciones interactivas y responsivas. Tiene una parte geo.
- [Infogram](https://infogram.com/): Crea gráficos de línea interactivos, gráficos circulares y de barra. O diseña visuales más complejas, como nubes de palabras o infografías
- [Dipity](http://www.dipity.com/): es una herramienta online que te permite organizar datos por tiempo, es como una línea del tiempo más interactiva
- [Google Fusion Tables](https://support.google.com/fusiontables/answer/2571232?hl=en): Para grandes cantidades de datos. Es un proyecto experimental que terminará en diciembre de 2019
- [Gapminder](https://www.gapminder.org/downloads/): Visualización temporal de datos estadísticos

---

## Herramientas *online* de análisis y visualización de datos mediante mapas
----

- [Carto](http://carto.com)
- [ESRI](http://esri.com)
- ...

---

class: center, middle

# Sistemas de Información Geográfica
----

---

## Sistemas de Información Geográfica
----

`Conjunto integrado de medios y métodos informáticos`, capaz de recoger, verificar, almacenar, gestionar, actualizar, manipular, recuperar, transformar, analizar, mostrar y transferir `datos espacialmente referidos a la Tierra`

---

## Sistemas de Información Geográfica
----

- El software [SIG](https://volaya.github.io/libro-sig/chapters/Introduccion_fundamentos.html) es una herramienta óptima de análisis para trabajar con `datos con componente geográfica`

- Herramienta de edición, análisis y visualización de datos

- La limpieza y estructuración de los datos debería ser enfocada a los formatos SIG

- Asegurando la salida de datos en formatos de estándares abiertos [OGC](http://www.opengeospatial.org/)

- Más de [50 años de desarrollo](https://volaya.github.io/libro-sig/chapters/Historia.html), con [opciones FOSS](http://www.osgeo.org/) desde 1985

---

## Sistemas de Información Geográfica
----
[Formatos](http://gisgeography.com/spatial-data-types-vector-raster/)
- `RASTER (*.tiff; *.bmp; *.jpg...)`: matriz de píxeles, con valores (entre 0 y 255, por ejemplo)
- `VECTORIAL (XML --> *.gml; *.kml; *.geojson; *.shp...)`: Tabla con entidades geográficas, definidas por puntos en una de las columnas + atributos
  - Puntos
  - Líneas (dos puntos) --> polilíneas (unión de lineas)
  - Polígonos: Polilínea cerrada cuyo punto inicial coincide con el punto final

---

## Sistemas de Información Geográfica
----
[Formatos](http://gisgeography.com/spatial-data-types-vector-raster/)

![Comparación entre los esquemas del modelo de representación vectorial (a) y ráster (b). Victor Olaya](./img/Esquemas_modelos_representacion.png)

*Comparación entre los esquemas del modelo de representación vectorial (a) y ráster (b). [Victor Olaya](https://volaya.github.io/libro-sig/chapters/Tipos_datos.html)*

---

## Operaciones SIG
----
- interoperabilidad e interconectividad
- Edición de capas en distintos formatos de documento
- conexión a bases de datos
- conexión a servicios - WMS, WFS, OpenStreetMap
- Transformación de formatos de capa (Shapefile, geoJSON, KML, GML...)

---

## Operaciones de Análisis SIG
----

- JOIN entre capas (código INE)
- Georeferenciación de imágenes --> Digitalización vectorial
- Calculadora de campos
- Álgebra de mapas
- Cálculo de redes

---

## Entorno de impresión (Layout)
----

Se definen la escala, el formato, tamaño y resolución de la imagen final

Se combina con la imagen del mapa rótulos, leyenda, escala gráfica...

Se exporta a distintos tipos de documento gráfico:
- imagen ráster: PNG, JPEG, geoTiff
- imagen vectorial escalable: SVG
- documentos PDF, geoPDF

---
## Herramientas de diseño gráfico
----

- Adobe Ilustrator
- Inkscape


---

class: center, middle

# SRE
## [Sistemas de Referencia Espacial]

---

## Sistemas de Referencia Espacial
----
Un `Sistema de Referencia` ("Reference System") [1] es una estructura geométrica para referir las coordenadas de puntos del espacio. Queda definido por
- la situación del origen
- las direcciones de los ejes
- la escala
- los algoritmos necesarios para sus transformaciones espaciales y temporales
- las constantes utilizadas en las definiciones y correcciones

---

## Sistemas de Referencia Espacial
----
Ten siempre presente el [sistema de referencia](http://www.ideandalucia.es/portal/iderap-portlet/content/300e9cf2-5fa1-471a-9885-26f36f68b9b7) de los datos que obtienes, así como de los datos que vas a representar

---

## Sistemas de Referencia Espacial
----

- Sistema Geodésico de Referencia (SGR)

Elipsoide + Datum --> Proyección

- Codificación [EPSG](http://www.epsg.io/)

---

## Proyecciones Cartogŕaficas
----

<iframe width="560" height="315" src="https://www.youtube.com/embed/pC4MwdqYeCI" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>

---

## Proyecciones Cartogŕaficas
----

Una proyección solo puede tener, como máximo una de estas propiedades:
- conforme: Conserva los ángulos
- equivalente: Conserva las superficies
- equidistante: Conserva las distancias

Las [proyecciones cartográficas](https://es.wikipedia.org/wiki/Proyecci%C3%B3n_cartogr%C3%A1fica):
- No son un adorno
- Tienen propiedades y limitaciones
- Siempre encontrarás la que necesitas

???

No es preciso conocer el proceso matemático que las ha generado para usarlas debidamente, al igual que para conducir un automóvil no es necesario dominar su mecánica.

Al usuario de datos geográficos le basta con saber que las proyecciones existen, que no son un adorno, que tienen propiedades y limitaciones, y que siempre podrán encontrar la más apropiada, con la seguridad de que la que precisan ya está inventada, porque hay muchas donde elegir - José Martín López

---

## Proyecciones Cartogŕaficas
----

![Resultado de aplicar cuatro proyecciones comunes a la representación de una cabeza humana](./img/proyecciones_cabeza_humana.jpg)

*[Resultado de aplicar distintas proyecciones a la representación de una cabeza humana](https://geoawesomeness.com/amazing-image-1921-will-explain-essence-map-projections/)*

 *“Elements of map projection with applications to map and chart construction”, 1921, by Charles H. Deetz and Oscar S. Adams*

---

## Sistema Geodésico Oficial en España
----
`(1852-1970)` Sistema de Referencia local
- Elipsoide de referencia de Struve
- Datum: Madrid
- Proyección poliédrica

---

## Sistema Geodésico Oficial en España
----
`(1970-2007)` Sistema Geodésico de Referencia ED50 (regional)
- Elipsoide internacional, de 1924. (Elipsoide de Hayford, 1909)
- Se toma el origen de latitudes el Ecuador, como origen de longitudes el meridiano de Greenwich y como Punto Astronómico Fundamental o Datum Fundamental, la torre de Helmert, en Postdam (entonces Alemania del Este)
- Proyección Universal Transversal Mercator (UTM)
- `EPSG 4230`, para coordenadas geográficas.
- `EPSG 230xx`, para UTM, donde “xx” es el huso correspondiente.


???
Tras la Segunda Guerra Mundial, el Coast and Geodetic Survey de los EEUU realizó una compensación de redes geodésicas de los países aliados con el fin de obtener una cartografía unificada, definiendo el Sistema Geodésico de Referencia ED50 ó European Datum 1950.

---

## Sistema Geodésico Oficial en España
----
`(2015-Act)` European Terrestrial Reference System 89 (ETRS89)
- Elipsoide GRS80 (Geodetic Reference System 1980)
- Se toma el origen de latitudes el Ecuador, como origen de longitudes el meridiano de Greenwich y como Punto Astronómico Fundamental o Datum Fundamental, la torre de Helmert, en Postdam (entonces Alemania del Este)
- Proyección Universal Transversal Mercator (UTM)
- `EPSG 4258`, para coordenadas geográficas.
- `EPSG 258xx`, para UTM, donde “xx” es el huso correspondiente.

---

## Sistema Geodésico Oficial en España
----
`(2015-Act)` European Terrestrial Reference System 89 (ETRS89)

Para cartografía terrestre, básica y derivada:
- a `escala igual o menor de 1:500.000`, se adopta el sistema de referencia de coordenadas [ETRS-Cónica Conforme de Lambert - ETRS89/LCC Europe](https://es.wikipedia.org/wiki/Proyecci%C3%B3n_conforme_de_Lambert) [EPSG:3034](https://epsg.io/3034)
- a `escalas mayores de 1:500.000`, se adopta el sistema de referencia de coordenadas `ETRS-Transversa de Mercator`

Para cartografía náutica se adopta la proyección Mercator

---

##Sistema Geodésico Oficial en España
----
[Hasta el 1 de enero de 2015](https://www.boe.es/buscar/doc.php?id=BOE-A-2007-15822) la información geográfica y cartográfica podía compilarse y publicarse en `cualquiera de los dos sistemas de referencia ED50 o ETRS89`, conforme a las necesidades de cada Administración Pública, siempre que las producciones en ED50 contengan la referencia a ETRS89.

A partir del 1 de enero de 2015, únicamente puede utilizarse el sistema de referencia ETRS89.

---

## Sistema de referencia PROYECTADO para web mapping
----

Una cosa es definir el sistema de coordenadas geográficas, y otra `la proyección`. Cuando vas a representar sobre una pantalla (o sobre el papel), es necesario proyectar sobre un plano.

Google Maps y el resto de APIs, como Leaflet, Bing, OpenStreetMap, etc, utilizan por defecto la `proyección Falso Mercator - WGS84` [EPSG 3857](https://epsg.io/3857)

Es un sistema de coordenadas PROYECTADO desde la superficie de una esfera a una superficie plana. El elipsoide de referencia es WGS84. Por convenio, las coordenadas que te da al consultar son en WGS84.

`WGS84` [EPSG: 4326](https://epsg.io/4326)

---

class: center, middle

# Generalidades del diseño cartográfico

---

## Elementos de un mapa: Título
----

![Genghis Khan't](./img/GenghisKhan.jpg)

???
Incluye:
  - Qué: el tema del mapa
  - Dónde: El área geográfica
  - Cuándo: información temporal

El tamaño de la letra del título debería ser dos o tres veces mayor que la del mapa.
Un subtítulo, algo más pequeño, es ideal para temás más complejos

---

## Elementos de un mapa: Texto explicativo

![El Agua en Andalucía](./img/DiaMundialDelAgua_01.jpg)

???
Muchas veces no puedes expresar todo lo que necesitas que se comprenda solo con el propio mapa.
Emplea bloques de texto en el mapa para comunicar información acerca del contenido del mapa, su contexto y sus objetivos.

En un mapa histórico, por ejemplo, incluye un párrafo demarcando el contexto histórico, e incluye bloques de texto en el mapa que cuenten qué ocurrió en los lugares importantes.

---

## Elementos de un mapa: Escala

![Cyclone Idahi Mozambique 05](./img/ElectricityConsumption.jpg)

???
- Desde escala local hasta escala continental, especialmente se si va a medir sobre el mapa:
  - La escala visual y verbal son más intuitivas
  - La escala numérica es más flexible
  - Si el usuario va a cambiar el tamaño del mapa (zoom), lo mejor es una escala visual

- Los mapas a escala pequeña (de todo el mundo o de una parte muy grande) no deberían incluir una escala visual simple, ya que estos mapas tienen importantes variaciones de escala

---

## Elementos de un mapa: Leyenda

![Queen Elizabeth II can be charged with a crime](./img/QueenElizabethCrime.jpg)

???
La leyenda puede variar muchísimo de un mapa a otro, pero debería incluir cualquier símbolo que creas que no sea familiar para la audiencia.
La leyenda es la clave para interpretar el mapa.

---

## Elementos de un mapa: Indicador de dirección

![Antarctica's Bays](./img/AntarcticaBays.jpg)


---

## Elementos de un mapa: Bordes
----

![](./img/strange_maps_scents_nyc.jpg)

---

## Elementos de un mapa: Fuente, créditos, etc
----

![Place Names in Iceland.jpg](./img/PlaceNamesIceland.jpg)


---

## Elementos de un mapa: Mapas de detalle y de localización

![CycloneIdahiMozambique_01.jpg](./img/CycloneIdahiMozambique_01.jpg)

???
When Cyclone Idai hit Mozambique, it caused floods up to six metres deep, leaving incredible devastation over a huge area, with homes, roads and bridges washed away

https://www.bbc.com/news/world-africa-47638696

---

class: center, middle

# Composición de un mapa

---

## Composición de un mapa: Recorrido de la vista
----

![](./img/composicion_01.jpg)
Making Maps. A Visual Guide to Map Design for GIS - John Krygier & Denis Wood
???
Asume que un mapa sigue un **recorrido**, como se lee la página de un libro, desde arriba a la izquierda hacia abajo a la derecha.
Compón las piezas del mapa de forma que lo que se verá primero esté en la esquina superior izquierda.


---
## Composición de un mapa: Centro Visual
----

![](./img/composicion_02.jpg)
Making Maps. A Visual Guide to Map Design for GIS - John Krygier & Denis Wood
???
El **centro visual** de un mapa se encuentra ligeramente por encima del centro real. Centrar implica dar importancia. Prueba a recolocarlas piezas de forma que lo más importante esté cerca del centro visual del mapa.
El lector puede que enfoque cerca de este centro y asuma que esos son los elementos más importantes.

---

## Composición de un mapa: Equilibrio
----

![](./img/composicion_03.jpg)
Making Maps. A Visual Guide to Map Design for GIS - John Krygier & Denis Wood
???
El **Equilibrio** (*balance*) se logra cuando todas las piezas se encuentran en su lugar. Estas piezas varían en peso, mientras algunas parecen tener más peso, otras son más ligeras. ¿Tiene tu composición apariencia equilibrada? A no ser que quieras sugerir al lector una falta de equilibrio...


---

## Composición de un mapa: Simetría
----

![](./img/composicion_04.jpg)
Making Maps. A Visual Guide to Map Design for GIS - John Krygier & Denis Wood
???
Podríamos definir la **simetría** como un equilibrio alrededor de un eje vertical.
Un equilibrio simétrico se entiende como tradicional, conservador y cauteloso.
La asimetría da sensación de moderno, progresivo, complejo y más creativo.

---

## Composición de un mapa: líneas de vista
----

![](./img/composicion_05.jpg)
Making Maps. A Visual Guide to Map Design for GIS - John Krygier & Denis Wood
???
Las **líneas de vista** (sight-lines) son líneas invisibles horizontales o verticales que tocan la parte más alta, la más baja o los lados de los elementos del mapa.
Minimizar el número de esas líneas reduce desajustes y realza la vista general. Esto anima al lector a entrar en el tema del mapa.

---

## Composición de un mapa: Cuadrículas
----
![](./img/composicion_06.jpg)
Making Maps. A Visual Guide to Map Design for GIS - John Krygier & Denis Wood

???
Cuadrículas simétricas están basadas en dos ejes centrales y márgenes a los lados, arriba y abajo.
Cuadrículas asimétricas son más complejas pero siguen dependiendo del centro visual mientras mantienen los márgenes a los lados, arriba y abajo.

---

class: center, middle

Otra manera de pensar en la imagen general del diseño de un mapa pueden ser las ideas de [Edward Tufte](https://www.edwardtufte.com)

[![](./img/strange_maps_scents_nyc.png)](https://archive.nytimes.com/www.nytimes.com/interactive/2009/08/29/opinion/20090829-smell-map-feature.html)

---
class: center, middle

# Cartografía temática
----

---
## Cartografía temática
----
- Mapas de puntos
- Mapas de símbolos proporcionales
- Mapas de coropletas
- Mapas de isolíneas
- Mapas de flujo
- Cartogramas
- Gráficos y diagramas

---
class: center

## Mapas de símbolos proporcionales
----
![contain image](./img/mapa_simbolosproporcionales.jpg)

*Mapa de Hipotecas. Atlas Nacional de España. IGN. 2004*

---
class: center

## Mapas de coropletas
----
![contain image](./img/mapa_coropletas.jpg)
----
*Mapa de Densidad de población. Atlas Nacional de España. IGN. 2005*

---
class: center

## Mapas de isolíneas
----
![contain image](./img/mapa_isolineas.jpg)
----
*Mapa de Humedad relativa media. Atlas Nacional de España. IGN. 2004*

---
class: center

## Mapas de flujo
----
![contain image](./img/mapa_flujo.jpg)
----
*Mapa de Principales flujos diarios interprovinciales de vehículos pesados por carretera. Atlas Nacional de España. IGN. 2006
]*

---
class: center

## Cartogramas
----
![contain image](./img/mapa_cartograma.jpg)
----
*Cartograma de la proyección de votos de las Elecciones presidenciales de Estados Unidos de 2008, basado en el voto popular, con cada rectángulo representando un voto. Wikipedia*
]

---

## Diseño Cartográfico
----
Cuando hablamos de diseño nos referimos a que se siguen unas normas, de legibilidad, de usabilidad...

- Simbolización y simplificación
- La misión del mapa es la de `facilitar la lectura y categorizar visualmente` la información georeferenciada
- Es importante conocer las [bases de simbolización cartográfica](http://geografiafisica.org/sem_2015_01/maestria_geom/SIG_p_GdR/elementos_diseno_cartografico/pdf_08_la_simbolizacion.pdf).
- No hace falta inventar la rueda: Las `variables del diseño cartográfico` ya están definidas y corroboradas (color, forma, tamaño, contraste, generalización de formas, etc.), según el tipo de dato que queramos dar (variables continuas, discretas...)
- Dos tazas de `creatividad`

???
En cartografía temática, donde prima el concepto de SINGULARIZACION, existe una amplia libertad de trabajo, ya que la normalización solo afecta a la estructura de la obra (paginación, tipos de paginas,...) y a la cartografía de base (generalmente en escalas entre 1: 2.000.000 y 1:10.000.000). Aquí, ante el mismo mapa, cien cartógrafos escogerán caminos distintos para resolver y presentar el mismo problema. Puede que técnicamente haya varias o muchas soluciones correctas, pero el adecuado uso de la simbología y del color, fruto, sin duda, del conocimiento teórico y de la experiencia, nos destacaran las soluciones adecuadas

---

class: center, middle

# Publicación
-----

---

## Publicación
----
Es importante `medir los recursos` de que se dispone
- Datos dinámicos `VS` estáticos
- Proceso de filtrado y cálculo de datos cada vez que se consulte `VS` mapa estático que alivie de carga los sistemas informáticos
- Servidor con base de datos geográfica `VS` documentos tabulados (JSON)
- `Servicios de terceros`, como [Carto](http://carto.com), [ESRI](http://esri.com), [Tableau](http://tableau.com), etc...
- `Desarrollo web`: HTML, CSS, Javascript, PHP...

Muchas veces un desarrollo interactivo es [`superfluo e innecesario`](https://www.goodreads.com/quotes/1185-those-are-my-principles-and-if-you-don-t-like-them-well), ya que una imagen estática bien diseñada sería suficiente, incluso más clara

???

Ojo con la disponibilidad y cambios de condiciones de los servicios a terceros a lo largo del tiempo


---
class: center

## Advertencias acerca de las *[3th parties](https://en.wikipedia.org/wiki/Third-party_software_component)*
----

![](./img/thereisnocloud-v2.jpg)

---
## Advertencias acerca de las *[3th parties](https://en.wikipedia.org/wiki/Third-party_software_component)*
----

[![problemas con google](./img/GoogleLimits.jpg)](http://www.juntadeandalucia.es/turismoydeporte/destinosturisticosaccesibles/es/mapa?provincia=All&municipio=All&field_recurso_tid=All&sort_by=title&sort_order=ASC)

---

## Advertencias acerca de las *[3th parties](https://en.wikipedia.org/wiki/Third-party_software_component)*
----
- `Cambios en las condiciones` del servicio
  - [Cambios en la API de Google Maps | Blog IDEE](http://blog-idee.blogspot.com/2018/06/cambios-en-la-api-de-google-maps.html)
  - [Google Maps ya no será gratuito… para desarrolladores | unocero](https://www.unocero.com/noticias/google-maps-ya-no-sera-gratuito-para-desarrolladores/)

- `Fin del servicio`
  - [Cloud Made - 2009](https://www.networkworld.com/article/2238946/data-center/cloudmade-pushing-open-source-alternative-to-google-maps.html)
  - [Valeo announced its acquisition of a 50% stake in the capital of CloudMade ](https://www.autoconnectedcar.com/2018/11/connected-car-news-tips-hyundai-microchip-bmw-bloom-elektrobit-trilumina-iteris-moovit-veniam-enevate-cypress-semi-valeo-cloudmade/)

- `Soberanía de los datos`
  - [Cómo afecta al usuario el cambio de condiciones de servicio de Google | Cinco Días](https://cincodias.elpais.com/cincodias/2018/12/17/companias/1545078650_866061.html)

---

## Advertencias acerca de las *[3th parties](https://en.wikipedia.org/wiki/Third-party_software_component)*
----

- `Distinta legislación` según el origen del servicio

- Imposible garantizar una `hemeroteca` en un medio periodístico

- `Publicidad injustificable ` en mapas publicados por instituciones públicas con *Google maps*.

---
class: center

## Web Mapping
----

![contain image](./img/teatrillo_titeres.png)

---

## Web Mapping
----
- Visualizador (Librería javascript)
  - [Openlayers](http://openlayers.org/)
  - [Leaflet](http://leafletjs.com/)...
- Mapa base
  - [OpenStreetMap](http:/openstreetmap.org)
  - [Google Maps](http://google.es/maps)
  - [servicios WMS](https://en.wikipedia.org/wiki/Web_Map_Service) como el del [PNOA](http://blog-idee.blogspot.com.es/2014/09/nuevo-servicio-wms-de-ortofotos-del-ign.html), ...
- Juego de datos a mostrar

---

class: center, middle

# Buenas Prácticas
---

## Buenas Prácticas
----
- Explica claramente en el título en qué consiste el mapa
- Indica siempre el `origen de las fuentes de datos`, así como su `fecha` --> `credibilidad`
- Añade la `descripción de la imagen` en el html final --> `accesibilidad`

```html
 <img src="mapa_01.gif" alt="Mapa de la proporción de colegios públicos de la comunidad de Valencia afectados por fibrocemento en su construcción" height="600" width="800">
```
- La accesibilidad de una web se mide según lo que puede leerse por máquina

???

Indicar la fuente dará credibilidad al gráfico

Buen título descriptivo

La accesibilidad de una web se mide según lo que puede leerse por máquina.

Hará que se indexe mejor la imagen, cara a consultas en buscadores

---

## Documenta
----
Un proyecto de análisis de datos ha de estar [perfectamente documentado](https://github.com/rdpeng/courses/blob/master/05_ReproducibleResearch/Checklist/index.md) para poder ser contrastado

- Utiliza un `sistema de control de versiones` durante todo el proceso (git, dropbox, por ejemplo)
- Documenta los procesos de análisis como `para poder repetirlos y obtener los mismos resultados`
- Indica siempre `el origen de las fuentes de datos`, así como su `fecha`
- `Cuantos más detalles mejor`
  - metodología de captura de datos
  - herramientas utilizadas (versión)
  - formato de los datos obtenidos.
  - etc
- Comparte tus resultados en `formatos abiertos`

---

class: center, middle

## Gracias por vuestra atención
![contain image](./img/cc-by-nc-sa.png)
- Slideshow created using [remark](http://github.com/gnab/remark)
.footnote[{{autor}} [@alayzappala](https://twitter.com/alayzappala)]

---

class: center, middle

# Lecturas

---

## Cultura General
----

- [Todos los mapas que conoces están mal | Jaime Rubio Hancock](https://verne.elpais.com/verne/2015/04/14/articulo/1429016086_681676.html)
- [La verdad sobre el mapa de Peters | Manu Arregi Biziola](http://naukas.com/2012/05/07/el-mapa-de-peters/)
- [This Amazing Image From 1921 Explains The Essence Of Map Projections | Aleks Buczkowski](https://geoawesomeness.com/amazing-image-1921-will-explain-essence-map-projections/)
- [El mundo de los mapas | International Cartographic Association (ICA)](http://www.ign.es/web/ign/portal/publicaciones-boletines-y-libros-digitales#DA-libro-elmundomapas)
- [Map School | vvaa](https://mapschool.io/index.es.html)
- [El Cartógrafo | Juan Mayorga](https://www.youtube.com/watch?v=WdNSl8F5xIw)

---

## SIG
----
- [Sistemas de Información Geográfica | Víctor Olaya](https://volaya.github.io/libro-sig/)
- [Use the Five-Step GIS Analysis Process | Esri Training Matters](https://blogs.esri.com/esri/esritrainingmatters/2009/10/08/use-the-five-step-gis-analysis-process/)

gvSIG
- [www.gvsig.org](http://gvsig.org)
- [Blog de gvSIG](https://blog.gvsig.org/)
- [Cátedra gvSIG](http://gvsig.edu.umh.es/category/gvsig/)

QGis
- [www.qgis.com](http://qgis.com)
- [Manuales y tutoriales de QGis](http://docs.qgis.org/2.14/en/docs/index.html)
- [Tutorial Layout QGis - Tim Sinnott](http://vcgi.vermont.gov/sites/vcgi/files/event_archive/sinnott_QGIS_Cartography.pdf)
- [Tutorial para Georeferenciar una imagen con QGIS](http://docs.qgis.org/1.8/en/docs/user_manual/plugins/plugins_georeferencer.html)

---

## Diseño Cartográfico
----
- [Sémiologie graphique. Les diagrammes, les réseaux, les cartes - Jacques Bertin](https://visionscarto.net/semiologia-grafica-bertin)
- [Making Maps. A Visual Guide to Map Design for GIS - John Krygier & Denis Wood](https://www.amazon.com/Making-Maps-Third-Visual-Design/dp/1462509983)
- [How to Lie with Maps - Mark S. Monmonier](http://www.markmonmonier.com/how_to_lie_with_maps_14880.htm)
- [Cartographic Design Principles | Ordnance Survey](https://www.ordnancesurvey.co.uk/resources/carto-design/carto-design-principles.html)
- [Principles of cartographic design | gislounge.com](https://www.gislounge.com/principles-of-cartographic-design/)

---

## Normativa
----
- [REAL DECRETO 1071/2007, de 27 de julio, por el que se regula el sistema geodésico de referencia oficial en España.](https://www.boe.es/buscar/doc.php?id=BOE-A-2007-15822)

- [Map Projections for Europe | European Commission](http://ec.europa.eu/eurostat/documents/4311134/4366152/Map-projections-EUROPE.pdf) pag.120

---

## Cuentas curiosas
----

- [@MapPorn](https://twitter.com/MapPornTweet)
- [@MapScaping](https://twitter.com/MapScaping)

- [@TerribleMaps](https://twitter.com/TerribleMaps)

---

class: center, middle

## Gracias por vuestra atención
![contain image](./img/cc-by-nc-sa.png)
- Slideshow created using [remark](http://github.com/gnab/remark)
.footnote[{{autor}} [@alayzappala](https://twitter.com/alayzappala)]

---

---
class: center, middle

# Ejercicio
----
##La presencia de fibrocemento en los colegios de Valencia

---

## Datos de partida
----

- Relació de centres educatius valencians que tenen instal·lacions amb fibrociment semblants a les del CEIP Ciutat de Bolonya: [Resposta a la sol·licitud de documentació presentada per Esquerra Unida a la Conselleria d'Educació i obtinguda -només- mitjançant sentència judicial del TSJCV (contenciós-administratiu 507/13)](http://fibrocimentnogracies.blogspot.com.es/p/mapa.html)

- Listado de todos los colegios, con coordenadas: [Portal de la Conselleria de Educación, Investigación, Cultura y Deporte](http://www.ceice.gva.es/web/centros-docentes/descarga-base-de-datos) de la Generalitat de València

- Listado de todos los colegios, por años: [Portal de datos abiertos](http://www.dadesobertes.gva.es/es/dataset?q=centres+educatius&sort=views_recent+desc) de la Generalitat de València

- Colegios en los que se está actuando: [Diario Información de Alicante](http://www.diarioinformacion.com/alicante/2017/02/09/educacion-quitara-curso-fibrocemento-once/1858676.html)

- Datos geográficos básicos del [centro de descargas del Instituto Geográfico Nacional](http://www.ign.es/)

---

## Datos de partida
----
[Descargar documento .ZIP](https://cartografo.es/presentacion/cartoperiodismo/data/Mapa_Escolar_Fibrocemento.zip)

---

##Instalar Plugins
----
--> `Complementos` --> `Administrar e instalar complementos` --> QuickMapServices


![Instalación del plugin de Openlayers](./img/10_ejercicio.jpg)

---


## Añadimos una capa con un servicio de mapas online
----

--> `Web` --> `QuickMapServices` --> `OSM` -- `OSM Standar`

![Vista de mapa con el servicio de Openstreetmap](./img/11_ejercicio.jpg)

---

## Formatos
----
Añadiremos el kml de los colegios construidos con fibrocementos en la comunidad de Valencia
http://fibrocimentnogracies.blogspot.com.es/p/mapa.html

`Capa` --> `Añadir capa` --> `Añadir capa Vectorial`

- `Encodig`: UTF-8
- `Source`: Mapa escolar del fibrociment.kmz

---

![Vista del mapa con la capa de los colegios afectados cargada](./img/12_ejercicio.jpg)

---
## Exportar a formato shapefile

--> Guardamos en formato ESRI (SHP)

--> colegios_fibrocemento.shp

![Ventana de exportación de capa](./img/13_ejercicio.jpg)
